package pobj.algogen;
public class PopulationFactory {
	/**
	* Operation permettant d'obtenir une nouvelle population generée  aleatoirement.
	* @param size la taille de la population à créer
	* @return une population composée de "size" individus génerés aléatoirement 
	*/
	public static Population createRandomPopulation (int size){
		Population pop =new Population();
		for (int i=0;i<size;i++){
			pop.add(new Individu());
		}
		return pop;
		
	
	}
	
}
