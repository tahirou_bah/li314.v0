package pobj.algogen;
import java.lang.Math;
import pobj.arith.EnvEval;
import pobj.arith.ExpressionFactory;
public class ValeurCible implements Environnement {
	private EnvEval env;
	private double cible;
	public ValeurCible (double cible,EnvEval env){
		this.cible =cible;
		this.env=env;
	}
	public ValeurCible (){
		this.cible=Math.random();
		this.env=ExpressionFactory.createRandomEnvEval();
	}
	
	public double eval(Individu i){
	//	Environnement env =new ValeurCible();
		//calcul la valeur de des variables
		double fit=i.getValeurPropre().eval(this.env);
		//calcul l'inverse du carré
		double fitness =(1/(Math.pow(fit,2)));
		i.setFitness(fitness);
		return i.getFitness();
	}
	public double getValue(){
		return  this.cible;
	}
	@Override
	public String toString (){
		return "valeur Cible= "+this.getValue();
	}
}
	

