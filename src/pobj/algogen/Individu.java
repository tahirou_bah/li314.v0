package pobj.algogen;
import java.util.Random;

import pobj.arith.Expression;
import pobj.arith.ExpressionFactory;
import pobj.arith.Operator;
 public class Individu implements Comparable<Individu>{
	
	 /**
	  * @author tb tahirou_bah
	  * private Expression valeurPropre;
	  * desormais la valeur propre d'un individu est expression et non un double
	  */
	 private Expression valeurPropre;
	private double fitness;
	public Individu () {
		Random generator =new Random();
		
		this.fitness =0;
		
		double valPropre = generator.nextDouble();
		this.valeurPropre = ExpressionFactory.createRandomExpression(generator.nextInt(3)) ;
	}
	public Individu (Expression valeurPropre){
		this.valeurPropre =valeurPropre;
		this.fitness=0;
	}
	public String toString() { return "Valeur Propre : " +this.getValeurPropre()+" sa fitness est " +this.getFitness();
	}
	public void setFitness (double fitness){
		this.fitness = fitness;
	}
	public Expression getValeurPropre(){
		return this.valeurPropre;
	}
	public void setValeurPropre(Expression valeurPropre){
		this.valeurPropre = valeurPropre;
	}
	public double getFitness(){
	return this.fitness;
	}
	/**
	 * 
	 */
	@Override
	public int compareTo(Individu ind){
		if(this.getFitness()==ind.getFitness())
			return 0;
		if(this.getFitness()<ind.getFitness())
			return 1;
		else
			return -1;
		
	}
	@Override
	public Individu clone(){
		 Individu ind=new Individu();
		 ind.setFitness(this.getFitness());
		 ind.setValeurPropre(this.getValeurPropre());
		
		return ind;
			
		}
	/**
	 * 
	 */
	protected void muter(){
		/*Random generator = new Random();
		double newValPropre=generator.nextInt(10);
		Expression exp_p=ExpressionFactory.createOperateurBinaire(this.getValeurPropre(),ExpressionFactory.createOperateurBinaire(this.getValeurPropre(),ExpressionFactory.createConstante(0.05),Operator.MULT),Operator.PLUS);
		Expression exp_m=ExpressionFactory.createOperateurBinaire(this.getValeurPropre(),ExpressionFactory.createOperateurBinaire(this.getValeurPropre(),ExpressionFactory.createConstante(0.05),Operator.MULT),Operator.MINUS);
		
		double m=this.getValeurPropre().-(0.05*this.getValeurPropre());
		if(newValPropre<m && newValPropre>p)
			this.setValeurPropre(newValPropre);*/
		valeurPropre=ExpressionFactory.createRandomExpression(2);
		
		}
	/**
	 * 
	 */
	protected Individu croiser(Individu ind){
		/*
		 * on crée une expression e2=e0+e1 ou  e0=this.getValeurPropre() et e1=ind.getValeur()
		 */
	Expression e2=	ExpressionFactory.createOperateurBinaire(getValeurPropre(), ind.getValeurPropre(), Operator.PLUS);
	/*
	 * on fait la moyenne de e3=e2/2; pour obtenir la moyenne
	 */
	Expression e3= ExpressionFactory.createOperateurBinaire(e2, ExpressionFactory.createConstante(2), Operator.DIV);
		Individu newInd=new Individu(e3);
		newInd.setFitness((this.getFitness()+ind.getFitness())/2);
		return newInd;
		
	}
 }
	
