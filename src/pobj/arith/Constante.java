package pobj.arith;

public class Constante implements Expression{

	private double value;
	public Constante(double value){
		this.value=value;
	}
	@Override
	public double eval(EnvEval enEval){
		return this.getValue();
	}
	public double getValue(){
		return this.value;
	}
	public String toString(){
		return " "+this.getValue();
	}
	@Override
	public Expression simplifier() {
		// TODO Auto-generated method stub
		return this;
	}
	
@Override
	public Expression clone(){
	return ExpressionFactory.createConstante(getValue());
	
}
}
