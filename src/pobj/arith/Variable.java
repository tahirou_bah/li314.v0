package pobj.arith;

public class Variable implements Expression {
	private int rang;
	/**
	 * 
	 * @param rang determine le rang de la variable dans la liste des variables
	 * avec le modificateur protected on rend la visiblité des constructeur au  package
	 * on cache aux client l'implementation des classes
	 * pour créer un onbjet il doit utiliser le factory
	 */
	protected Variable(int rang){
		this.rang=rang;
	}
	/**
	 * @return return une chaine qui contient le rang de la variable
	 */
	@Override
	public String toString(){
		return "X"+this.rang;
	}
	/**
	 * @author tb tahirou_bah
	 * @param env de type EnvEval donne l'environnement qui va permettre d'evaluer l'expression
	 */
	@Override
	public double eval(EnvEval env) {
		
		return env.getValue(rang);
	}
	@Override
	public Expression simplifier() {
		// TODO Auto-generated method stub
		return ExpressionFactory.createVariable(rang);
	}
	@Override
	public Expression clone(){
		return ExpressionFactory.createVariable(rang);
	}
	

}
