package pobj.arith;

public interface Expression {
	public double eval(EnvEval envEval);
	public Expression clone();
	public Expression simplifier();
}
