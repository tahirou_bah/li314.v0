package pobj.arith;

public enum Operator {
	/**
	 * @author tb tahirou_bah
	 * ces quatre attributs determine l'ensemble des operations de base que l'on peut faire sur une expression 
	 * DIV pour l'operateur division /
	 * PLUS pour l'operateur d'addition +
	 * MULT pour la multiplication *
	 * MINUS pour la soustraction -
	 */
	DIV,
	MINUS,
	PLUS,
	MULT
}
