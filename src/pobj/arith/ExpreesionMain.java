package pobj.arith;

public class ExpreesionMain {

	/**
	 * @param args
	 * @author tb tahirou_bah
	 * cette classe permet donc de tester les calsses definies dans le package arith
	 */
	public static void main(String[] args) {
		
		//System.out.println(ExpressionFactory.createConstante(0.9));
		//System.out.println(ExpressionFactory.createVariable(1));
		//System.out.println(ExpressionFactory.createOperateurBinaire(ExpressionFactory.createVariable(1), 
			//	ExpressionFactory.createConstante(0.9),
				//Operator.MULT));
		Expression exp=ExpressionFactory.createRandomExpression(2);
		EnvEval envEval=ExpressionFactory.createRandomEnvEval();
		System.out.println("#################################");
		Expression var1=ExpressionFactory.createVariable(0);
		Expression var2 =ExpressionFactory.createVariable(1);
		Expression exp2= ExpressionFactory.createOperateurBinaire(var1, var2, Operator.PLUS);
		System.out.println(exp2);
		System.out.println("############################################");
		exp.eval(envEval);
		System.out.println(exp2.eval(envEval));
		
		}

}
